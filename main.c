#include <stdio.h>

#include "Rtv1.h"

t_point     make_re(double x, double y, double z)
{
    t_point new;

    new.x = x;
    new.y = y;
    new.z = z;
    return (new);
}

void			imgpp(void *mlx, void *img, int x, int y, int rgb)
{
    int				bpp;
    int				si;
    int				e;
    char			*im;
    unsigned int	tmp;

    im = mlx_get_data_addr(img, &bpp, &si, &e);
    tmp = mlx_get_color_value(mlx, rgb);
    if (x >= 0 && x < W && y >= 0 && y < H)
    {
        ft_memcpy((void *)((im + y * W *
                                 (bpp / 8) + x * (bpp / 8))), (void *)&tmp, 4);
    }
}

double deg2rad(double deg)
{
    return (deg * 3.14159265359 / 180);
}

double     shine(t_point light, t_point cam, t_point norm)
{
    t_point h;
    double l;

    h = scalar_div(sum_vector(light, cam), abs_vec(sum_vector(light, cam)));
    l = dot_product(h, norm) / (abs_vec(h) * abs_vec(norm));
    return (l);
}



int main() {

    void *mlx;
    void *img;
    void *win;
    t_point dota;



    mlx = mlx_init();
    win = mlx_new_window(mlx, W, H, "gopa");
    img = mlx_new_image(mlx, W, H);


    double w;
    double h;
    int i = 0;
    int j = 0;
    t_point cam;
    t_point c_dir;
    t_sphere s;
    t_ray ray;
    double inter;
    t_point int_point;

    s.center.x = 0;
    s.center.y = 0;
    s.center.z = 0;
    s.radius = 60.;
    cam.x = -100;
    cam.y = 0;
    cam.z = 0;
    c_dir.x = 1;
    c_dir.y = 0;
    c_dir.z = 0;

    double inter_p;
    t_plane plane;

    double inter_c;
    t_cone cone;

    plane.p = make_re(10, 0, 0);
    plane.norm =  make_re(1, 0, 0);

    cone.p = make_re(0, 0, 0);
    cone.v = make_re(0, 1, 0);
    cone.a = deg2rad(30.0);

    double n = 10.0;

    w = n * tan(deg2rad(30.0) / 2.); // fov
    h = ((double)W / (double)H) * n * tan(deg2rad(30.0) / 2.); // fov
    while (i <= W)
    {
        j = 0;

        while (j <= H)
        {

            t_point X = norm_vector(c_dir);
            t_point Y = (t_point){0., 1., 0.};       //ordinates
            t_point Z = norm_vector(cross_product(Y, X));

			double dis_j = j - (double)W / 2.;
			double dis_i = i - (double)H / 2.;


            dota = (t_point) {
                (double)dis_i  * Y.x + (double)dis_j * Z.x + X.x,
                (double)dis_i  * Y.y + (double)dis_j * Z.y + X.y,
                (double)dis_i  * Y.z + (double)dis_j * Z.z + X.z
            };
            dota = norm_vector(sub_vector(dota, cam));

            ray.origin = cam;
            ray.dir = dota;
            if ((inter = inter_ray_and_sphere(s, ray)) > 0)
            {
                int_point = sum_vector(cam, scalar_prod(ray.dir, inter));
                double shine_color = shine(norm_vector(sub_vector(make_re(100, 0, 100), int_point)), norm_vector(sub_vector(cam, int_point)), norm_vector(sphere_normal(s, int_point)));
                double diffuse = get_light_color(int_point, norm_vector(sphere_normal(s, int_point)));
                imgpp(mlx, img, i, j, color_mutiply(0xffffff,  diffuse - shine_color));
            }
            if ((inter_p = inter_plane(ray, plane)) > 0)
            {
                int_point = sum_vector(cam, scalar_prod(ray.dir, inter_p));
                double shine_color = shine(norm_vector(sub_vector(make_re(100, 0, 100), int_point)), norm_vector(sub_vector(cam, int_point)), plane.norm);
                double diffuse = get_light_color(int_point, plane.norm);
                imgpp(mlx, img, i, j, color_mutiply(0xff0000,  diffuse - shine_color));
            }
            if ((inter_c = inter_cone(ray, cone)) > 0)
            {
                int_point = sum_vector(cam, scalar_prod(ray.dir, inter_c));
                double diffuse = get_light_color(int_point, norm_vector(norm_cone(cone, int_point)));
                imgpp(mlx, img, i, j, color_mutiply(0xff0000,  diffuse));
            }
            else
                imgpp(mlx, img, i, j, 0x000000);
            j++;
        }
        i++;
    }
    mlx_put_image_to_window(mlx, win, img, 0, 0);
    mlx_loop(mlx);
    return (0);
}
