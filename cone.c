/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppavlich <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/06 20:53:34 by ppavlich          #+#    #+#             */
/*   Updated: 2018/09/06 20:53:36 by ppavlich         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Rtv1.h"

t_point		norm_cone(t_cone cone, t_point int_p)
{
	t_point	v;
	t_point	r;
	t_point	n;

	v = sub_vector(int_p, cone.p);
	r = sub_vector(v, scalar_prod(cone.v, dot_product(cone.v, v)));
	n = cross_product(cross_product(v, r), r);
	return (n);
}

double		get_t(double a, double b, double c)
{
	double	t1;
	double	t2;
	double	d;

	if ((d = b * b - 4 * a * c) < 0)
		return (d);
	t1 = (-b - sqrt(d)) / (2 * a);
	t2 = (-b + sqrt(d)) / (2 * a);
	if ((t1 <= t2 && t1 >= 0) || (t1 >= 0 && t2 < 0))
		return (t1);
	if ((t2 <= t1 && t2 >= 0) || (t2 >= 0 && t1 < 0))
		return (t2);
	return (-1);
}

double	inter_cone(t_ray ray, t_cone cone)
{
	double 	a;
	double	b;
	double	c;
	t_point	delp;

	delp = sub_vector(ray.origin, cone.p);
	a = (((1 + cos(cone.a * 2)) / 2)  * pow(abs_vec(sub_vector(ray.dir , \
		scalar_prod(cone.v, dot_product(ray.dir, cone.v)))), 2)) - 
		((1 - cos(cone.a * 2)) / 2) * pow(dot_product(ray.dir, cone.v), 2);
	b = 2 * ((1 + cos(cone.a * 2)) / 2) * dot_product(sub_vector(ray.dir , \
		scalar_prod(cone.v, dot_product(ray.dir, cone.v))),\
		sub_vector(delp, scalar_prod(cone.v, dot_product(delp, cone.v)))) - \
			(2 * ((1 - cos(cone.a * 2)) / 2) * dot_product(ray.dir, cone.v) * \
				dot_product(delp, cone.v));
	c = ((1 + cos(cone.a * 2)) / 2) * pow(abs_vec(sub_vector(delp, \
		scalar_prod(cone.v, dot_product(delp, cone.v)))), 2) - \
		(((1 - cos(cone.a * 2)) / 2) * pow(dot_product(delp, cone.v), 2));
	return(get_t(a, b, c));
}

