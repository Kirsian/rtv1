
#include "Rtv1.h"

int	inter_plane(t_ray ray, t_plane plane)
{

	double	t;
	double	a;
	double	b;

	a = dot_product(sub_vector(ray.origin, plane.p), plane.norm);
	b = dot_product(ray.dir, plane.norm);
	if (b == 0 || (a < 0 && b < 0) || (a > 0 && b > 0))
		return (-1);
	t = -a / b;
	return (t > 0 ? t : -1);
}