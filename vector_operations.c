/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*    vector_operations.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knikanor <knikanor@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 18:35:46 by knikanor          #+#    #+#             */
/*   Updated: 2018/07/25 12:13:42 by knikanor         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Rtv1.h"

double		abs_vec(t_point vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}

t_point     scalar_div(t_point v3, double n)
{
    return (make_re(v3.x / n, v3.y / n, v3.z / n));
}

t_point		norm_vector(t_point vector)
{
	double len;

	len = abs_vec(vector);
	if (len == 0)
		len = 1;
	vector.x /= len;
	vector.y /= len;
	vector.z /= len;
	return (vector);
}

t_point		sub_vector(t_point vector1, t_point vector2)
{
	t_point res;

	res.x = vector1.x - vector2.x;
	res.y = vector1.y - vector2.y;
	res.z = vector1.z - vector2.z;
	return (res);
}

t_point		sum_vector(t_point vector1, t_point vector2)
{
	t_point res;

	res.x = vector1.x + vector2.x;
	res.y = vector1.y + vector2.y;
	res.z = vector1.z + vector2.z;
	return (res);
}

double		dot_product(t_point v1, t_point v2)
{
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

//t_point	ort_to_vector(double t, t_point *orth)
//{
//	t_point res;
//
//	res.x = t * ort->x;
//	res.y = t * ort->y;
//	res.z = t * ort->z;
//	return (res);
//}

//t_point	norm_vector(t_point *vector)
//{
//	double n;
//
//	n = dot_product(vector, vector);
//	if (n != 0)
//		n = 1.0f / sqrt(n);
//	return (vector_scale(n, vector));
//}


//double		vector_length(t_point vector1, t_point vector2)
//{
//	double product;
//
//
//	product = dot_product(vector1, vector2);
//	return (sqrt(product));
//}

t_point		scalar_prod(t_point v, double n)
{
	t_point res;

	res.x = v.x * n;
	res.y = v.y * n;
	res.z = v.z * n;

	return (res);
}


t_point	cross_product(t_point v1, t_point v2)
{
	t_point gopa;

	gopa.x = v1.y * v2.z - v1.z * v2.y;
	gopa.y = v1.z * v2.x - v1.x * v2.z;
	gopa.z = v1.x * v2.y - v1.y * v2.x;
	return (gopa);
}

