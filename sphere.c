/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: knikanor <knikanor@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 16:39:42 by knikanor          #+#    #+#             */
/*   Updated: 2018/07/25 12:15:52 by knikanor         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Rtv1.h"

t_point sphere_normal(t_sphere s, t_point intersect)
{
	return (sub_vector(s.center, intersect));
}

double	inter_ray_and_sphere(t_sphere s, t_ray ray)
{
	double d;
	double sqrt_d;
	t_point dist;
	t_res res;

	dist = sub_vector(ray.origin, s.center);
	res.a = dot_product(ray.dir, ray.dir);
	res.b = 2 * dot_product(ray.dir, dist);
	res.c = dot_product(dist, dist) - s.radius * s.radius;
	d = (res.b * res.b) - 4 * res.a * res.c;
	if (d < 0)
		return (0);
	sqrt_d = sqrt(d);
	res.x1 = (-res.b + sqrt_d) / (2 * res.a);
	res.x2 = (-res.b - sqrt_d) / (2 * res.a);
	if (res.x1 > res.x2)
        res.x1 = res.x2;
	return (res.x1);
}

