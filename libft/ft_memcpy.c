/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 20:31:34 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 15:18:26 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char		*nd;
	unsigned const char	*nc;
	size_t				i;

	i = 0;
	nd = (unsigned char *)dst;
	nc = (unsigned const char *)src;
	while (i < n)
	{
		nd[i] = nc[i];
		i++;
	}
	return (dst);
}
