/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:04:45 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/28 17:04:45 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdlib.h"

void	*ft_memalloc(size_t size)
{
	char	*arr;
	size_t	i;

	if (!(arr = malloc(sizeof(char) * size)))
		return (NULL);
	i = 0;
	while (i < size)
	{
		arr[i] = 0;
		i++;
	}
	return ((void *)arr);
}
