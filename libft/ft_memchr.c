/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 21:00:15 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/01 21:00:15 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memchr(const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*tmp;

	tmp = (unsigned char*)src;
	i = -1;
	while (++i < n)
		if (tmp[i] == (unsigned char)c)
			return ((void*)&tmp[i]);
	return (NULL);
}
