/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 18:15:38 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/22 18:15:39 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str, const char *str1, size_t n)
{
	unsigned long	i;
	int				j;

	i = -1;
	while ((++i != n) && (str1[i] != '\0' || str[i] != '\0'))
	{
		j = 0;
		while (str[i + j] == str1[j] && i + j <= n)
		{
			if (str1[j + 1] == '\0')
				return ((char*)&str[i]);
			j++;
		}
		if (str[i] == '\0')
			break ;
	}
	if (str1[0] == '\0')
		return ((char*)str);
	return (NULL);
}
