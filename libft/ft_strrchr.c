/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 17:37:15 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/22 17:37:15 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	int len;

	len = -1;
	while (str[++len] != '\0')
		;
	while (str[len] != c && len >= 0)
		len--;
	if (str[len] == c)
		return ((char *)&str[len]);
	return (NULL);
}
