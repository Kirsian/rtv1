/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 18:49:05 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/28 18:49:05 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strnew(size_t size)
{
	char		*arr;
	size_t		i;

	arr = NULL;
	if (size != 0)
	{
		if (!(arr = malloc(sizeof(char) * size + 1)))
			return (NULL);
		i = -1;
		while (++i <= size)
			arr[i] = '\0';
	}
	return (arr);
}
