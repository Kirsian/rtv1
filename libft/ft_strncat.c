/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 13:38:11 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/24 13:38:13 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *str1, const char *str2, size_t n)
{
	int		len;
	size_t	i;

	len = (int)ft_strlen(str1);
	i = -1;
	while (++i < n && str2[i] != '\0')
	{
		str1[len] = str2[i];
		len++;
	}
	str1[len] = '\0';
	return (str1);
}
