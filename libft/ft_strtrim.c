/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 15:49:36 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/29 15:49:37 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		skip(const char *s)
{
	int			i;

	i = 0;
	while (((s[i] >= 9 && s[i] <= 13) || s[i] == ' ') && (s[i] != '\0'))
		i++;
	return (i);
}

static int		lskip(const char *s)
{
	int			i;

	i = ft_strlen(s) - 1;
	while (((s[i] >= 9 && s[i] <= 13) || s[i] == ' ') && (s[i] > 0))
		i--;
	return (i + 1);
}

static char		*ret(void)
{
	char *str;

	str = (char *)malloc(sizeof(char));
	str[0] = '\0';
	return (str);
}

char			*ft_strtrim(char const *s)
{
	char		*result;
	size_t		fp;
	size_t		lp;
	int			i;

	if (s == 0)
		return (NULL);
	fp = skip(s);
	lp = lskip(s);
	if (fp == ft_strlen(s))
		return (ret());
	result = (char *)malloc(sizeof(char) * (lp - fp) + 1);
	if (!result)
		return (NULL);
	i = 0;
	while (fp + i < lp)
	{
		result[i] = s[fp + i];
		i++;
	}
	result[i] = '\0';
	return (result);
}
