#ifndef RTV1_H
# define RTV1_H
# define W 1000
# define H 1000
# define MIN(a, b) ((a) < (b) ? (b) : (a))

# include "libft/libft.h"
# include "stdlib.h"
# include "fcntl.h"
# include "math.h"
# include "minilibx_macos/mlx.h"
# include <time.h>
# include <stdio.h>

typedef  struct s_rgb
{
    int r;
    int g;
    int b;
}               t_rgb;

typedef struct	s_point
{
    double		x;
    double		y;
    double		z;
}				t_point;

typedef struct s_ray
{
    t_point origin;
    t_point dir;
}              t_ray;

typedef struct s_sphere
{
    t_point center;
    double radius;
}               t_sphere;

typedef struct s_plane
{
    t_point     p;
    t_point     norm;
}              t_plane;

typedef struct s_cone
{
    t_point     p;
    t_point     v;
    double      a;
}              t_cone;

typedef struct s_res
{
    double x1;
    double x2;
    double t;
    double a;
    double b;
    double c;
}              t_res;

t_point		sub_vector(t_point vector1, t_point vector2);
t_point	    ort_to_vector(double t, t_point *orth);
double		vector_length(t_point vector1, t_point vector2);
t_point		scalar_prod(t_point v, double n);
t_point		sum_vector(t_point vector1, t_point vector2);
t_point	    cross_product(t_point v1, t_point v2);
double		inter_ray_and_sphere(t_sphere s, t_ray ray);
double		dot_product(t_point vector1, t_point vector2);
t_point		norm_vector(t_point vector);
t_point     sphere_normal(t_sphere s, t_point intersect);
double		abs_vec(t_point vec);
t_point     make_re(double x, double y, double z);
t_point     scalar_div(t_point v3, double n);
int         inter_plane(t_ray ray, t_plane plane);
double      inter_cone(t_ray ray, t_cone cone);
t_point     norm_cone(t_cone cone, t_point int_p);


#endif