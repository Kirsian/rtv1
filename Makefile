NAME = rtv

IDIR =

VPATH = src:include

LIB = libft/libft.a

SRCS =	main.c				\
		sphere.c            \
		vector_operations.c \
		plane.c				\
		cone.c				\
		color.c				\


F = -Wall -Wextra -Werror -I$(IDIR) -O3 -g

FLAGS = -lmlx -framework OpenGL -framework AppKit

BINS = $(SRCS:.c=.o)

all: $(NAME) run

$(NAME): $(BINS)
	make -C libft/
	gcc -o $(NAME) $(BINS) $(F) $(FLAGS) $(LIB)

%.o: %.c
	gcc $(F) -c -o $@ $<

clean:
	make -C libft/ clean
	/bin/rm -f $(BINS)

fclean: clean
	make -C libft/ fclean
	/bin/rm -f $(NAME)

re: fclean all

run:
	./$(NAME)
