/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppavlich <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/22 23:01:40 by ppavlich          #+#    #+#             */
/*   Updated: 2018/09/22 23:01:43 by ppavlich         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Rtv1.h"

double get_light_color(t_point inter, t_point norm)
{
    t_point light;
    t_point l_dir;
    double  cos;

    light.x = 100;
    light.y = 0;
    light.z = 100;
    l_dir = sub_vector(light, inter);
    l_dir = norm_vector(l_dir);
    cos = 1 * MIN(dot_product(l_dir, norm), 0);
    return (cos);
}

int         color_mutiply(int color, double cos)
{
    t_rgb c;
    c.r = (color >> 16) & 0xFF;
    c.g = (color >>  8) & 0xFF;
    c.b = (color >>  0) & 0xFF;
    c.r = color_clamp(c.r * cos);
    c.g = color_clamp(c.g * cos);
    c.b = color_clamp(c.b * cos);
    return ((int)(((c.r) << 16) + ((c.g) << 8) + c.b));
}

unsigned char		color_clamp(int color)
{
	if (color < 0)
		return (0);
	if (color > 254)
		return (254);
	return ((unsigned char)color);
}
